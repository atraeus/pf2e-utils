import './utils.js';
import Modifiers from "./modifiers.js"
import {constants} from "./modifiers.js";
import './combat.js';

class PF2eMacrosClass{
	constructor(){
		PF2eUtils.registerGMAction("GMHuntPrey", gmActionHuntPrey);
		PF2eUtils.registerGMAction("GMInspire", gmActionInspire);
	}
	
	//Hunt Prey macro
	huntPrey(sourceToken){
		const targetToken = game.user.targets.values().next().value;
		PF2eUtils.triggerMessage(constants.GM_ACTION, {
			action: "GMHuntPrey",
			args: [sourceToken.data.name, targetToken?targetToken.id:null]
		});
	}
	
	//Cast Inspire macro
	castInspire(sourceToken, effectIds){
		const heroicsDC = 14 + 5 + sourceToken.actor.level + parseInt(sourceToken.actor.level/3);
		const lingeringDC = 14 + sourceToken.actor.level + parseInt(sourceToken.actor.level/3);
		let dialogContent = "<div style='display:flex;justify-content: space-around;'>"
		// inspire courage/defense radio options
		dialogContent += "<div class='inspire-types' style='display:flex;flex-direction:column;'>" +
			"<div>" + 
				"<input id='" + constants.INSPIRE_COURAGE + "' type='radio' name='inspire' checked style='margin:0 5px;'>" +
				"<label for='" + constants.INSPIRE_COURAGE + "'>Courage</label>" + 
			"</div>" + 
			"<div>" + 
				"<input id='" + constants.INSPIRE_DEFENSE + "' type='radio' name='inspire' style='margin:0 5px;'>" + 
				"<label for='" + constants.INSPIRE_DEFENSE + "'>Defense</label>" + 
			"</div>" + 
		"</div>";

		// inspire heroics or lingering composition
		dialogContent += `<div class='inspire-metamagic'style='display:flex;flex-direction:column;'>
			<div style='display:flex; align-items:center;'>
				<input id="${constants.INSPIRE_HEROICS}" class='inspire-heroics' type='checkbox' style="width:12px; height:12px; flex-basis:12px;">
				<label for="${constants.INSPIRE_HEROICS}">Inspire Heroics (DC ${heroicsDC})</label>
			</div>
			<div style='display:flex; align-items:center;'>
				<input id="${constants.LINGERING_COMPOSITION}" class='lingering-composition' type='checkbox' style="width:12px; height:12px; flex-basis:12px;">
				<label for="${constants.LINGERING_COMPOSITION}">Lingering Composition (DC ${lingeringDC})</label>
			</div>
		</div></div>`;

		dialogContent += "<hr/><div class='pc-list' style='display:flex; flex-wrap:wrap; padding-bottom:5px;'>";
		const gridSize = game.scenes.active.grid;
		const pcs = PF2eUtils.getAllCharacterTokens();
		pcs.forEach(pc => {
			const dist = canvas.grid.measureDistances([{"ray": new Ray(sourceToken.center, pc.center)}], {gridSpaces: true})[0];
			const id = "inspire_" + pc.id;
			dialogContent += "<div style='display:flex; align-items:center; box-shadow: 1px 1px 4px 0px #888888; margin-bottom:5px; width:50%;'><input id='" + id + "' type='checkbox' " + (dist <= 60 ? "checked" : "") + "><label for='" + id + "'>" + pc.name + "</label></div>";
		});
		dialogContent += "</div>";

		const onLingeringChange = e => {
			if (e.currentTarget.checked)
				$("#"+constants.INSPIRE_HEROICS)[0].checked = false
		}
		const onHeroicsChange = e => {
			if (e.currentTarget.checked)
				$("#"+constants.LINGERING_COMPOSITION)[0].checked = false
		}

		let action;
		const inspireDialog = new Dialog({
			title: "Inspire Courage/Defense",
			content: dialogContent,
			buttons: {
				yes: {
				  icon: "<i class='fas fa-check'></i>",
				  label: "Inspire",
				  callback: () => action = "inspire"
				},
				no: {
				  icon: "<i class='fas fa-times'></i>",
				  label: "Remove",
				  callback: () => action = "remove"
				}
			},
			default: "yes",
			close: html => {
				const type = $(html).find("div.inspire-types input[type='radio']:checked").map(function(){return this.id;}).get(0);
				const heroics = $(html).find("div.inspire-metamagic input[type='checkbox'].inspire-heroics")[0].checked;
				const lingering = $(html).find("div.inspire-metamagic input[type='checkbox'].lingering-composition")[0].checked;
				if (action === "inspire" && heroics) {
					const dc = {
						value: heroicsDC,
					};
					sourceToken.actor.skills.performance.roll({
						dc: dc,
						callback: roll => {
							let modifier;
							switch (roll.degreeOfSuccess) {
								case 0:
								case 1:
								default:
									modifier = 1;
									break;
								case 2:
									modifier = 2;
									break;
								case 3:
									modifier = 3;
									break;
							}

							PF2eUtils.triggerMessage(constants.GM_ACTION, {
								action: "GMInspire",
								args: [
									action,
									type,
									// sourceToken.data.name,
									modifier,
									effectIds,
									1,
									$(html).find("div.pc-list input[type='checkbox']:checked").map(function() {return this.id.substring(8);}).get()
								]
							});
						}
					});
				}
				else if (lingering){
					const dc = {
						value: lingeringDC,
					};
					sourceToken.actor.skills.performance.roll({
						dc: dc,
						callback: roll => {
							let rounds;
							switch (roll.degreeOfSuccess) {
								case 0:
								case 1:
								default:
									rounds = 1;
									break;
								case 2:
									rounds = 3;
									break;
								case 3:
									rounds = 4;
									break;
							}

							PF2eUtils.triggerMessage(constants.GM_ACTION, {
								action: "GMInspire",
								args: [
									action,
									type,
									// sourceToken.data.name,
									1,
									effectIds,
									rounds,
									$(html).find("div.pc-list input[type='checkbox']:checked").map(function() {return this.id.substring(8);}).get()
								]
							});
						}
					});
				}
				else {
					PF2eUtils.triggerMessage(constants.GM_ACTION, {
						action: "GMInspire",
						args: [
							action,
							type,
							// sourceToken.data.name,
							1,
							effectIds,
							1,
							$(html).find("div.pc-list input[type='checkbox']:checked").map(function() {return this.id.substring(8);}).get()
						]
					});
				}
				$(document).off("change", "#"+constants.LINGERING_COMPOSITION, onLingeringChange)
				$(document).off("change", "#"+constants.INSPIRE_HEROICS, onHeroicsChange)
			}
		}).render(true);

		$(document).on("change", "#"+constants.LINGERING_COMPOSITION, onLingeringChange)
		$(document).on("change", "#"+constants.INSPIRE_HEROICS, onHeroicsChange)
	}
}

/**
 * Adds/removes hunt prey status to target token
 * @param {string} source - name of the actor who used Hunt Prey
 * @param {string} target - id of the token which to apply Hunt Prey to.
 */
async function gmActionHuntPrey(source, target){
	let targetToken;
	
	//Remove any existing Hunt Preys from source while searching for new target token
	TokenLayer.instance.objects.children.forEach(async token => {
		if ((token.actor.data.data.customModifiers['hunt-prey'] || []).some(modifier => modifier.label === "Hunt Prey: " + source)) {
			await token.actor.removeCustomModifier('hunt-prey', "Hunt Prey: " + source);

			if (token.data.effects.includes("systems/pf2e/icons/features/classes/hunt-prey.webp"))
				await token.toggleEffect("systems/pf2e/icons/features/classes/hunt-prey.webp");
		}
		if (token.id === target)
			targetToken = token;
	});

	//If target token does not exist, then exit.
	if (!targetToken)
		return;
	
	//Apply customModifier and tiny icon to target token
	await targetToken.actor.addCustomModifier("hunt-prey", "Hunt Prey: " + source, 0, "status");
	if (!targetToken.data.effects.includes("systems/pf2e/icons/features/classes/hunt-prey.webp"))
		await targetToken.toggleEffect("systems/pf2e/icons/features/classes/hunt-prey.webp");
}

const InspireEffectIds = [
	"Compendium.pf2e.spell-effects.beReeFroAx24hj83", //Inspire Courage +1
	"Compendium.pf2e.spell-effects.kZ39XWJA3RBDTnqG", //Inspire Courage w/ Inspire Heroics +2
	"Compendium.pf2e.spell-effects.VFereWC1agrwgzPL", //Inspire Courage w/ Critical Inspire Heroics +3
	"Compendium.pf2e.spell-effects.DLwTvjjnqs2sNGuG", //Inspire Defense +1
	"Compendium.pf2e.spell-effects.Chol7ExtoN2T36mP", //Inspire Defense w/ Inspire Heroics +2
	"Compendium.pf2e.spell-effects.BKam63zT98iWMJH7"  //Inspire Defense w/ Critical Inspire Heroics +3
]

/**
 * Adds/removes status bonuses for Inspire Courage with GM privileges
 * @param {string} action - Either "remove" or "inspire"
 * @param {string} type - Either "inspire_courage" or "inspire_defense"
 * @param {number} modifier - modifier value, default to +1
 * @param {string} effectIds - modifier value, default to +1
 * @param {number} rounds - duration of inspire, default to 1
 * @param {array <string>} targets - Array of actor ids to be affect by inspire
 */
function gmActionInspire(action, type, modifier, effectIds, rounds, targets){
	const pcs = PF2eUtils.getAllCharacterTokens();
	pcs.forEach(async pc => {
		for (let effectId of Object.values(effectIds)){
			await PF2eUtils.applyEffect(false, effectId, pc.actor, rounds)
		}
		if (action === "inspire" && targets.includes(pc.id))
			await PF2eUtils.applyEffect(true, effectIds[`${type}${modifier}`], pc.actor, rounds)
	});
}

window.PF2eMacros = new PF2eMacrosClass();