import './utils.js';
import Modifiers from "./modifiers.js"
import {constants} from "./modifiers.js";

class PF2eEffectsMacrosClass{
	constructor(){
		PF2eUtils.registerGMAction("GMHuntPrey", gmActionHuntPrey);
		PF2eUtils.registerGMAction("GMInspire", gmActionInspire);
	}
	
	huntPrey(sourceToken){
		const targetToken = game.user.targets.values().next().value;
		PF2eUtils.triggerMessage(constants.GM_ACTION, {
			action: "GMHuntPrey",
			args: [sourceToken.data.name, targetToken?targetToken.id:null]
		});
	}
	
	castInspire(sourceToken){
		let dialogContent = "<div style='display:flex; flex-wrap:wrap; padding-bottom:5px;'>";
		const gridSize = game.scenes.active.data.grid;
		const pcs = PF2eUtils.getAllCharacterTokens();
		pcs.forEach(pc => {
			const dist = canvas.grid.measureDistances([{"ray": new Ray(sourceToken.center, pc.center)}], {gridSpaces: true})[0];
			const id = "inspire_" + pc.id;
			dialogContent += "<div style='display:flex; align-items:center; box-shadow: 1px 1px 4px 0px #888888; margin-bottom:5px; width:50%;'><input id='" + id + "' type='checkbox' " + (dist <= 60 ? "checked" : "") + "><label for='" + id + "'>" + pc.data.name + "</label></div>";
		});
		dialogContent += "</div>";

		// inspire courage/defense/competence checkboxes
		dialogContent += "<hr/>" + 
			"<div class='inspire-types' style='display:flex;justify-content: space-around;'>" + 
				"<div style='display:flex; align-items:center;'>" + 
					"<input id='" + constants.INSPIRE_COURAGE + "' type='checkbox'><label for='" + constants.INSPIRE_COURAGE + "'>Courage</label>" + 
				"</div>" + 
				"<div style='display:flex; align-items:center;'>" + 
					"<input id='" + constants.INSPIRE_DEFENSE + "' type='checkbox'><label for='" + constants.INSPIRE_DEFENSE + "'>Defense</label>" + 
				"</div>" + 
			"</div>";

		let action;
		new Dialog({
			title: "Inspire Courage/Defense",
			content: dialogContent,
			buttons: {
				yes: {
				  icon: "<i class='fas fa-check'></i>",
				  label: "Inspire",
				  callback: () => action = "inspire"
				},
				no: {
				  icon: "<i class='fas fa-times'></i>",
				  label: "Remove",
				  callback: () => action = "remove"
				}
			},
			default: "yes",
			close: html => {
				const types = $(html).find("div.inspire-types input[type='checkbox']:checked").map(function(){return this.id;}).get();
				PF2eUtils.triggerMessage(constants.GM_ACTION, {
					action: "GMInspire",
					args: [
						action,
						types,
						sourceToken.data.name,
						$(html).find("input[type='checkbox']:checked").map(function() {return this.id.substring(8);}).get()
					]
				});
			}
		}).render(true);
	}
	
	raiseShield(sourceToken){
		const hasModifier = PF2eUtils.actorHasCustomModifier(sourceToken.actor, constants.RAISE_SHIELD);
		applyCustomModifiers(!hasModifier, constants.RAISE_SHIELD, sourceToken);
	}
	
	async rage(sourceToken){
		const hasModifier = PF2eUtils.actorHasCustomModifier(sourceToken.actor, constants.RAGE);
		const applied = await applyCustomModifiers(!hasModifier, constants.RAGE, sourceToken);

		let tempHP = sourceToken.actor.data.data.details.level.value + sourceToken.actor.data.data.abilities.con.mod;
		if (sourceToken.actor.data.data.attributes.hp.temp > tempHP)
			tempHP = sourceToken.actor.data.data.attributes.hp.temp;
		await sourceToken.actor.update({"data.attributes.hp.temp":applied?tempHP:0});
	}
}

/**
 * Adds/removes hunt prey status to target token
 * @param {string} source - name of the actor who used Hunt Prey
 * @param {string} target - id of the token which to apply Hunt Prey to.
 */
async function gmActionHuntPrey(source, target){
	let targetToken;
	
	//Remove any existing Hunt Preys from source while searching for new target token
	TokenLayer.instance.objects.children.forEach(async token => {
		if ((token.actor.data.data.customModifiers['hunt-prey'] || []).some(modifier => modifier.name === "Hunt Prey: " + source)) {
			await token.actor.removeCustomModifier('hunt-prey', "Hunt Prey: " + source);

			if (token.data.effects.includes("systems/pf2e/icons/features/classes/hunt-prey.jpg"))
				await token.toggleEffect("systems/pf2e/icons/features/classes/hunt-prey.jpg");
		}
		if (token.id === target)
			targetToken = token;
	});

	//If target token does not exist, then exit.
	if (!targetToken)
		return;
	
	//Apply customModifier and tiny icon to target token
	await targetToken.actor.addCustomModifier("hunt-prey", "Hunt Prey: " + source, 0, "status");
	if (!targetToken.data.effects.includes("systems/pf2e/icons/features/classes/hunt-prey.jpg"))
		await targetToken.toggleEffect("systems/pf2e/icons/features/classes/hunt-prey.jpg");
}

/**
 * Adds/removes status bonuses for Inspire Courage with GM privileges
 * @param {string} action - Either "remove" or "inspire"
 * @param {array <string>} types - Either "inspire_courage" or "inspire_defense"
 * @param {string} source - name of the actor who casted inspire
 * @param {array <string>} targets - Array of actor ids to be affect by inspire
 */
function gmActionInspire(action, types, source, targets){
	const pcs = PF2eUtils.getAllCharacterTokens();
	if (action === "remove") {
		pcs.forEach(async(pc) => {
			for (const type of types) {
				await applyCustomModifiers(false, type, pc, [source]);
			}
		});
	}
	else if (action === "inspire") {
		pcs.forEach(async(pc) => {
			for (const type of types) {
				await applyCustomModifiers(targets.includes(pc.id), type, pc, [source]);
			}
		});
	}
}

/**
 * Adds/removes custom modifiers
 * @param {boolean} apply - Whether to add or remove
 * @param {string} type - Modifier to use, based on constants
 * @param {Token} token - Token to update
 * @param {Object[]} params - Extra parameters needed to determine modifier data
 * @returns {boolean} True if modifiers were added; false if they were removed
 */
async function applyCustomModifiers(apply, type, token, params) {
	const hasRequirements = Modifiers[type].hasRequirements(token.actor);
	const data = Modifiers[type].getData(token.actor, ...(params ?? []));
	const customModifiers = PF2eUtils.buildCustomModifiers(apply && hasRequirements, token.actor, data);
	await token.actor.update({"data.customModifiers": customModifiers});
	await PF2eUtils.updateIcon(type, token, customModifiers);
	return apply && hasRequirements;
}

window.PF2eEffectsMacros = new PF2eEffectsMacrosClass();
