export const constants = Object.freeze({
	GM_ACTION: "gm_action",
	UPDATE_REACTIONS: "update_reactions",
	INSPIRE_COURAGE: "inspire_courage",
	INSPIRE_DEFENSE: "inspire_defense",
	INSPIRE_HEROICS: "inspire_heroics",
	LINGERING_COMPOSITION: "lingering_composition",
});

const modifiers = {};
modifiers[constants.INSPIRE_COURAGE] = {
	name: "Inspire Courage",
	icon: "systems/pf2e/icons/spells/inspire-courage.webp",
	hasRequirements: function(actor) {
		// Every actor can accept this buff
		return true;
	},
	getData: function(actor, source, modifier) {
		const name = this.name + ": " + source;
		return [
			{stat:"attack", name:name, value:modifier ?? 1, type:"status"},
			{stat:"damage", name:name, value:modifier ?? 1, type:"status"}
		];
	}
};
modifiers[constants.INSPIRE_DEFENSE] = {
	name: "Inspire Defense",
	icon: "systems/pf2e/icons/spells/inspire-defense.webp",
	hasRequirements: function(actor) {
		// Every actor can accept this buff
		return true;
	},
	getData: function(actor, source, modifier) {
		const name = this.name + ": " + source;
		return [
			{stat:"ac", name:name, value:modifier ?? 1, type:"status"},
			{stat:"fortitude", name:name, value:modifier ?? 1, type:"status"},
			{stat:"reflex", name:name, value:modifier ?? 1, type:"status"},
			{stat:"will", name:name, value:modifier ?? 1, type:"status"}
		];
	}
};

export default Object.freeze(modifiers)