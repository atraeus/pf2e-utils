import {constants} from "./modifiers.js";

// When the combat tracker is rendered, we need to add custom HTML changes
Hooks.on('renderCombatTracker', async (app, html, options) => {
    // If there's as combat, we can proceed.
    if (game.combat && html) {
        game.combat.combatants.forEach(c => {
            if (!c.token)
                return;

            // Display the reaction tracker HTML
            if (PF2eUtils.isTokenPC(c.token) || game.user.isGM) {
                let $reaction = $('<div class="reaction-wrapper" style="display:flex; align-items:center;"></div>');
                createReactionSpan($reaction, "Reaction", "modules/PF2eUtils/icons/Reaction.png", c.token);

                if (PF2eUtils.actorHasFeat(c.actor, "Quick Shield Block")) {
                    createReactionSpan($reaction, "Shield_Block", "modules/PF2eUtils/icons/Shield.png", c.token);
                }
                if (PF2eUtils.actorHasFeat(c.actor, "Combat Reflexes")) {
                    createReactionSpan($reaction, "Attack_of_Opportunity", "modules/PF2eUtils/icons/Sword.png", c.token);
                }

                html.find(`.combatant[data-combatant-id="${c.id}"]`).find('.combatant-controls').append($reaction);
            }
        });
    }
});

const createReactionSpan = ($element, name, icon, token) => {
    const reactionSpan = $("<span class='" + name +"' style='display:flex; align-items:center; margin-right:6px;' title='" + name + "'>" +
    "<div style='background-image:url(" + icon + "); background-size:contain; background-repeat:no-repeat; background-position:right; width:16px; height:16px; margin-right:4px;'></div>" +
    "<span style='display:inline-block'><i class='" + (token.getFlag("PF2eUtils", name)?"fas":"far") + " fa-circle' style='font-size:13px;text-decoration:none !important; text-shadow:none; color:#888;'></i></span>" +
    "</span>");
    $element.append(reactionSpan);

    // if owner of this combatant, allow for clicks to use reactions and change color to white
    const combatant = game.combat.combatants.find(c => c.token && c.token.id === token.id);
    if (combatant.isOwner || game.user.isGM) {
        reactionSpan.on("click", (event) => {
            token.setFlag("PF2eUtils", name, token.getFlag("PF2eUtils", name)?false:true).then(() => {
                PF2eUtils.triggerMessage(constants.UPDATE_REACTIONS, {tokenId: token.id});
                Hooks.call('PF2eUtils_updateReactions', combatant);
            });
            event.stopPropagation();
        });
        reactionSpan.find("i").css("color", "#fff");
    }
};

Hooks.on('PF2eUtils_updateReactions', combatant => {
    if (game.combat) {
        const $combatant = $(`.combatant[data-combatant-id="${combatant._id}"]`);
        updateReactionSpan($combatant, combatant.token, "Reaction");
        if (PF2eUtils.actorHasFeat(combatant.actor, "Quick Shield Block")) {
            updateReactionSpan($combatant, combatant.token, "Shield_Block");
        }
        if (PF2eUtils.actorHasFeat(combatant.actor, "Combat Reflexes")) {
            updateReactionSpan($combatant, combatant.token, "Attack_of_Opportunity");
        }
    }
});

const updateReactionSpan = ($combatant, token, name) => {
    const reactionSpan = $combatant.find(`.combatant-controls .${name}`);
    if (token.getFlag("PF2eUtils", name))
        reactionSpan.find("i").removeClass("far").addClass("fas");
    else
        reactionSpan.find("i").addClass("far").removeClass("fas");
};

Hooks.on("updateCombat", combat => {
    if (!game.user.isGM)
        return;
    if (combat.current.round === 1 && combat.current.turn === 0)
        beginCombat();
    else if (combat.current.round !== 1)
        resetCombatantReactions(combat.combatant);
});

const beginCombat = () => {
    game.combat.combatants.forEach(combatant => {
        resetCombatantReactions(combatant);
    });
};

const resetCombatantReactions = (combatant) => {
	if (!combatant)
		return;
    combatant.token.setFlag("PF2eUtils", "Reaction", true).then( () => {
        combatant.token.setFlag("PF2eUtils", "Shield_Block", true).then( () => {
            combatant.token.setFlag("PF2eUtils", "Attack_of_Opportunity", true).then( () => {
                PF2eUtils.triggerMessage(constants.UPDATE_REACTIONS, {tokenId: combatant.token.id});
                Hooks.call('PF2eUtils_updateReactions', combatant);
            })
        })
    })
};