import Modifiers from "./modifiers.js"
import {constants} from "./modifiers.js";

class PF2eUtilsClass {
	constructor(){
		Hooks.on('ready', function(){
			game.socket.on("module.PF2eUtils", function(message){
				this._onSocketMessage(message);
			}.bind(this));
		}.bind(this));

		this.gmActions = {};

		this._onSocketMessage = function(message){
			switch(message.action)
			{
				case constants.GM_ACTION:
					//Check if this user is the assigned GM to execute this action
					if (game.user.id !== message.data.GMID)
						return;

					this.gmActions[message.data.action].apply(this, message.data.args);
					break;
				case constants.UPDATE_REACTIONS:
					if (game.combat) {
						const combatant = game.combat.combatants.find(c => c.tokenId === message.data.tokenId);
						if (combatant)
							Hooks.call('PF2eUtils_updateReactions', combatant);
					}
					break;
			}
		};

		this.registerGMAction = function(action, callback){
			this.gmActions[action] = callback;
		};
	}
	
	triggerMessage(action, data){
		if (action === constants.GM_ACTION){
			//If the user is a GM execute the action.  No need to go over websocket
			if (game.user.isGM)
			{
				this.gmActions[data.action].apply(this, data.args);
				return;
			}
			//If the user is not a GM. Find a GM user to execute the action
			const gmUser = game.users.filter(user => user.isGM && user.active)[0];
			if (!gmUser)
				return false;
			data.GMID = gmUser.id;
		}
		game.socket.emit("module.PF2eUtils", { action: action, data: data});
	}
	
	getSelectedTokens(){
		return TokenLayer.instance.controlled;
	}
	
	getTokenByName(name){
		return TokenLayer.instance.objects.children.find(token => token.actor.name == name);
	}
	
	getAllCharacterTokens(){
		let tokens = TokenLayer.instance.objects.children.filter(token => this.isTokenPC(token));
		return tokens.sort((a,b) => a.name.localeCompare(b.name));
	}

	isTokenPC(token) {
		if (token && token.actor)
			return token.actor.type === "character";
		return false;
	}
	
	async createCombatEncounter(excludeList){
		//let scene = game.scenes.viewed;
		//if ( !scene ) return;
		let scene = game.scenes.current;
		if ( !scene ) return;
		const cls = getDocumentClass("Combat");
		const combat = await cls.create({scene: scene?.id});
		await combat.activate({render: false});

		let characterTokens = this.getAllCharacterTokens();
		let combatants = [];
		for (let token of characterTokens){
			if (Array.isArray(excludeList) && excludeList.includes(token.actor.name))
				continue;
			combatants.push({tokenId: token.id, hidden: false});
		}
		await ui.combat.combats[ui.combat.combats.length-1].createEmbeddedDocuments("Combatant", combatants);
	}
	
	async addSelectedTokensToCombatEncounter(hidden){
		let rollIniativeList = [];
		let selectedTokens = this.getSelectedTokens();
		let combatants = [];
		for (let token of selectedTokens){
			if (token.inCombat)
				continue;
			combatants.push({tokenId: token.id, hidden: hidden});
		}
		await ui.combat.combats[ui.combat.combats.length-1].createEmbeddedDocuments("Combatant", combatants);
		ui.combat.combats[ui.combat.combats.length-1].rollNPC({skipDialog:true});
	}

	actorHasFeat(actor, featName){
		const feat = actor.items.filter(item => item.type === 'feat')
            .filter(featItem => featItem.name === featName);
		return feat.length>0;
	}

	async applyEffect(apply, effectId, actor, rounds){
		const source = (await fromUuid(effectId)).toObject();
		source.flags = mergeObject(source.flags ?? {}, { core: { sourceId: effectId } });
		if (rounds)
			source.system.duration.value = rounds

		const existing = actor.itemTypes.effect.find((e) => e.flags.core?.sourceId.split(".").slice(-1)[0] === effectId?.split(".")?.slice(-1)[0]);
		if (!apply && existing)
			await existing.delete();
		else if (apply && !existing)
			await actor.createEmbeddedDocuments("Item", [source]);
	}
}

window.PF2eUtils = new PF2eUtilsClass;